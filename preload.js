require("./lib/logging").hook();
const modulesContext = require.context(
  "./ext",
  true,
  /^\.\/[a-zA-Z0-9]+\/index.js$/
);
const defaultConfig = require("./default-config.json");

rpc.exports = {
  init: async () => {
    try {
      Java.perform(() => {
        const FileInputStream = Java.use("java.io.FileInputStream");
        const FileWriter = Java.use("java.io.FileWriter");
        const Scanner = Java.use("java.util.Scanner");
        const ApplicationProvider = Java.use(
          "com.discord.utilities.lifecycle.ApplicationProvider"
        );
        const Environment = Java.use("android.os.Environment");

        const App = ApplicationProvider.INSTANCE.value.application.value;
        global.spiggy = {
          instances: {App},
          packageName: App.getPackageName(),
          resolvedModules: {},
        };

        const fileBase = Environment.getExternalStoragePublicDirectory(
          Environment.DIRECTORY_DOWNLOADS.value
        );
        spiggy.configPath = `${fileBase.getAbsolutePath()}/spiggy/${
          spiggy.packageName
        }.json`;
        let configData;
        console.log(`Loading config from ${spiggy.configPath}`);

        try {
          const configInput = FileInputStream.$new(spiggy.configPath);
          const scanner = Scanner.$new(configInput);
          configData = "";
          while (scanner.hasNextLine()) {
            configData += scanner.nextLine() + "\n";
          }
        } catch (err) {
          console.error("Couldn't load config!", err.stack);
          configData = JSON.stringify(defaultConfig, null, 2) + "\n";
          const configWriter = FileWriter.$new(spiggy.configPath);
          configWriter.write(configData, 0, configData.length);
          configWriter.close();
        }
        const config = JSON.parse(configData);

        spiggy.getModuleOptions = (name) => config.modules[name]?.options;

        for (const moduleName in config.modules) {
          const module = config.modules[moduleName];
          if (module?.enabled || module === true) {
            console.log("Loading", moduleName);
            loadModule(moduleName);
          }
        }

        for (const moduleName in spiggy.resolvedModules) {
          try {
            spiggy.resolvedModules[moduleName].init();
          } catch (err) {
            console.error(`Couldn't init ${moduleName}: ${err.stack}`);
          }
          console.log(`Initted ${moduleName}!`);
        }
      });
    } catch (err) {
      console.error(err.stack);
    }
  },
};

function loadModule(moduleName) {
  try {
    const ext = modulesContext(`./${moduleName}/index.js`);
    spiggy.resolvedModules[moduleName] = ext;
  } catch (err) {
    console.error(`Error loading ${moduleName}!`, err.stack);
    return;
  }
  const dependencies = spiggy.resolvedModules[moduleName].dependencies;
  if (dependencies) {
    for (const dependency of dependencies) {
      loadModule(dependency);
    }
  }
}
