module.exports = {
  init: () => {
    const Adjust = Java.use("com.adjust.sdk.Adjust");
    Adjust.setEnabled(false);
    const AdjustConfig = Java.use(
      "com.discord.utilities.analytics.AdjustConfig"
    );
    AdjustConfig.init.implementation = function () {};

    const FirebaseCrashlytics = Java.use(
      "com.google.firebase.crashlytics.FirebaseCrashlytics"
    );
    FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(false);

    const AnalyticSuperProperties = Java.use(
      "com.discord.utilities.analytics.AnalyticSuperProperties"
    );
    AnalyticSuperProperties.INSTANCE.value.setAdvertiserId(
      "00000000-0000-0000-0000-000000000000"
    );
    AnalyticSuperProperties.INSTANCE.value.setAdvertiserId.implementation =
      function () {};

    const AnalyticsUtils$Tracker = Java.use(
      "com.discord.utilities.analytics.AnalyticsUtils$Tracker"
    );
    for (let i = 0; i < AnalyticsUtils$Tracker.track.overloads.length; ++i) {
      AnalyticsUtils$Tracker.track.overloads[i].implementation = function (
        ...args
      ) {};
    }
    AnalyticsUtils$Tracker.drainEventsQueue.implementation = function () {
      if (!this.eventsQueue.value.isEmpty()) {
        console.error(
          "Store attempted to drain queue... Contents:",
          this.eventsQueue.value.size()
        );
        const top = this.eventsQueue.value.peek();
        console.error("Top entry was", top);
        this.eventsQueue.value.clear();
      }
    };
    const RestAPI = Java.use("com.discord.utilities.rest.RestAPI");
    RestAPI.science.implementation = function (science) {
      console.error(
        "Blocked attempted track! This indicates a bug in pseudoScience!",
        science
      );
    };
  },
};
