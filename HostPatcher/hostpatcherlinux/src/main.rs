#[macro_use]
extern crate log;
use clap::{App, Arg};
use env_logger;
use libhostpatcher::APK;
use std::fs;

fn main() {
  env_logger::init();

  let matches = App::new("hostpatcherlinux")
    .version("0.1.0")
    .author("Mary Strodl <ipadlover8322@gmail.com>")
    .about("Patcher for spiggy")
    .arg(
      Arg::new("APK")
        .help("APK file to pull data from")
        .required(true)
        .index(1),
    )
    .arg(
      Arg::new("OUTPUT")
        .help("File to write resulting APK to")
        .required(true)
        .index(2),
    )
    .arg(
      Arg::new("package-name")
        .help("Package name to give generated APK")
        .default_value("tech.coolmathgames.spiggy")
        .long("package-name")
        .short('p')
        .takes_value(true),
    )
    .arg(
      Arg::new("display-name")
        .help("Display name to give generated APK")
        .default_value("spiggy")
        .long("display-name")
        .short('n')
        .takes_value(true),
    )
    .arg(
      Arg::new("icon")
        .help("Path to icon to give generated APK")
        .long("icon")
        .short('i')
        .takes_value(true),
    )
    .arg(
      Arg::new("only-architecture")
        .help("Only generate libraries for the specified architecture")
        .long("only-architecture")
        .takes_value(true),
    )
    .arg(
      Arg::new("preload")
        .help(
          "Path to preload JS file or 'listen' to make gadget start a server on localhost:27042",
        )
        .default_value("/sdcard/Download/spiggy/preload.js")
        .long("preload")
        .alias("preload-path")
        .takes_value(true),
    )
    .get_matches();

  let original = matches.value_of("APK").unwrap().to_string();
  let output = matches.value_of("OUTPUT").unwrap().to_string();
  let display_name = matches.value_of("display-name").unwrap().to_string();
  let package_name = matches.value_of("package-name").unwrap().to_string();

  let architectures = matches
    .value_of("only-architecture")
    .map(|architecture| vec![architecture.to_string()]);

  let preload_path = match matches.value_of("preload").unwrap() {
    "listen" => None,
    path => {
      if !path.starts_with("/") {
        error!("Invalid path, only absolute paths are allowed: {}", path);
        std::process::exit(1);
      } else {
        Some(path)
      }
    }
  };

  let icon = match matches.value_of("icon") {
    Some(icon_path) => match fs::read(icon_path) {
      Ok(icon) => icon,
      Err(err) => {
        error!("Couldn't open {}: {:?}", icon_path, err);
        std::process::exit(1);
      }
    },
    None => include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/default-icon.png")).to_vec(),
  };

  let file = fs::File::open(original).unwrap();
  let output_file = fs::File::create(output).unwrap();
  let mut apk = APK::new(file, output_file).unwrap();
  apk
    .patch(
      &package_name,
      &display_name,
      Some(icon),
      preload_path,
      None,
      architectures,
    )
    .unwrap();
}
