#[macro_use]
extern crate log;
use serde_json::json;
use std::error::Error;
use std::io::{Read, Seek, SeekFrom, Write};
use std::vec::Vec;
use zip;
use zip::CompressionMethod;
mod axml;
mod binary_patch;
mod gadget;

pub struct APK<R: Read + Seek, W: Write + Seek> {
  file: R,
  output: W,
}

const CRC_LIST: [u32; 4] = [146037806, 2392735449, 2993301796, 3981144232];

impl<R: Read + Seek, W: Write + Seek> APK<R, W> {
  pub fn new(file: R, output: W) -> Result<Self, Box<dyn Error + Send + Sync>> {
    Ok(APK { file, output })
  }
  // base_package specifies new package name (original is com.discord)
  pub fn patch(
    &mut self,
    base_package: &str,
    _display_name: &str,
    icon: Option<Vec<u8>>,
    preload_path: Option<&str>,
    cache_dir: Option<String>,
    architectures: Option<Vec<String>>,
  ) -> Result<(), Box<dyn Error + Send + Sync>> {
    let options = zip::write::FileOptions::default().compression_method(CompressionMethod::Stored);
    let mut gadget = gadget::Gadget::new(cache_dir);
    self.file.seek(SeekFrom::Start(0))?;
    self.output.seek(SeekFrom::Start(0))?;

    {
      let mut zip_reader = zip::ZipArchive::new(&mut self.file)?;
      let mut writer = zip::ZipWriter::new(&mut self.output);
      debug!("Zip has {} entries", zip_reader.len());

      let picked_abi = match architectures {
        Some(architectures) => {
          let mut lowest = architectures.len();
          for i in 0..zip_reader.len() {
            let file = zip_reader.by_index(i)?;
            let name = file.name();
            if name.starts_with("lib/") && name.ends_with("/libdsti.so") {
              let architecture = &name["lib/".len()..name.len() - "/libdsti.so".len()].to_string();
              if let Some(index) = architectures.iter().position(|r| r == architecture) {
                if lowest > index {
                  lowest = index;
                  if lowest == 0 {
                    break;
                  }
                }
              }
            }
          }
          if architectures.len() == lowest {
            None
          } else {
            Some(architectures[lowest].clone())
          }
        }
        None => None,
      };
      debug!("Targetting ABI: {:?}", picked_abi);

      for i in 0..zip_reader.len() {
        let mut file = zip_reader.by_index(i)?;
        let name = file.name();
        if name.starts_with("lib/") && name.ends_with("/libdsti.so") {
          let architecture = &name["lib/".len()..name.len() - "/libdsti.so".len()].to_string();

          if let Some(ref picked_abi) = picked_abi {
            if picked_abi != architecture {
              continue;
            }
          }

          match gadget.pull_gadget(architecture) {
            Ok(mut gadget) => {
              writer.start_file_aligned(name, options, 4)?;
              debug!("Found {}", name);
              let patched_elf = binary_patch::inject_binary(file)?;
              writer.write_all(&patched_elf)?;
              debug!(
                "Wrote patched ELF! Writing lib/{}/libgadget.so",
                architecture
              );
              writer.start_file_aligned(format!("lib/{}/libgadget.so", architecture), options, 4)?;
              std::io::copy(&mut gadget, &mut writer)?;

              if let Some(preload_path) = preload_path {
                writer.start_file_aligned(format!("lib/{}/libgadget.config.so", architecture), options, 4)?;
                writer.write_all(
                  json!({
                    "interaction": {
                      "type": "script",
                      "path": preload_path,
                    },
                  })
                  .to_string()
                  .as_bytes(),
                )?;
              }
            }
            Err(err) => {
              if let Some(err_down) = err.downcast_ref::<gadget::GadgetError>() {
                match err_down {
                  gadget::GadgetError::MissingArchitecture => continue,
                  _ => return Err(err),
                };
              } else {
                return Err(err);
              }
            }
          }
        } else if name == "AndroidManifest.xml" {
          debug!("Located AndroidManifest.xml!");
          let mut manifest: Vec<u8> = Vec::new();
          file.read_to_end(&mut manifest)?;
          writer.start_file_aligned("AndroidManifest.xml", options, 4)?;
          writer.write_all(&axml::AXML::patch(manifest, base_package.clone())?)?;
        } else if name == "resources.arsc" {
          debug!("Located resources.arsc!");
          let mut resources: Vec<u8> = Vec::new();
          file.read_to_end(&mut resources)?;
          writer.start_file_aligned("resources.arsc", options, 4)?;
          writer.write_all(&axml::AXML::update_arsc(
            resources,
            "com.discord".to_string(),
            base_package.to_string(),
          )?)?;
        } else if name.starts_with("META-INF/") {
          // debug!("Ignoring META-INF: {}", name);
        } else {
          if let Some(ref icon) = icon {
            if CRC_LIST.contains(&file.crc32()) {
              debug!("Found a match! Replacing {} with new icon!", name);
              writer.start_file_aligned(name, options, 4)?;
              writer.write_all(icon)?;
              continue;
            }
          }
          // debug!("Copying {}", name);
          writer.raw_copy_file(file)?
        }
      }
      writer.finish()?;
      debug!("Finalized APK!");
    }

    Ok(())
  }
}
