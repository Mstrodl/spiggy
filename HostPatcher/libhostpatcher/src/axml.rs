use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};
use std::error::Error;
use std::fmt;
use std::io::Cursor;
use std::iter::Iterator;
use std::mem::size_of;

#[derive(Debug)]
pub enum AXMLError {
  MissingNullByte,
}

impl std::error::Error for AXMLError {}

impl fmt::Display for AXMLError {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    return match self {
      AXMLError::MissingNullByte => f.write_str("Missing null byte in string"),
    };
  }
}

pub struct AXML {}

struct Replacement {
  from: String,
  to: String,
}

#[allow(dead_code)]
struct ResChunkHeader {
  r#type: u16,
  header_size: u16,
  size: u32,
}

#[allow(dead_code)]
struct StringPoolHeader {
  string_count: u32,
  style_count: u32,
  flags: u32,
  strings_start: u32,
  styles_start: u32,
}

#[allow(dead_code)]
struct ResTablePackage {
  id: u32,
  name: [u16; 128],
  type_strings: u32,
  last_public_type: u32,
  key_strings: u32,
  last_public_key: u32,
  type_id_offset: u32,
}

const RES_TABLE_TYPE: u16 = 0x0002;
const RES_XML_TYPE: u16 = 0x0003;

const RES_STRING_POOL_TYPE: u16 = 0x0001;
// RES_TABLE_TYPE
const RES_TABLE_PACKAGE_TYPE: u16 = 0x0200;

const UTF8_FLAG: u32 = 1 << 8;

impl AXML {
  pub fn patch(data: Vec<u8>, base_package: &str) -> Result<Vec<u8>, Box<dyn Error + Send + Sync>> {
    let mut reader = Cursor::new(data);

    let mut replacements = vec![
      Replacement {
        from: "android.permission.READ_EXTERNAL_STORAGE".to_string(),
        to: "android.permission.MANAGE_EXTERNAL_STORAGE".to_string(),
      },
      Replacement {
        from: "com.discord".to_string(),
        to: base_package.to_string(),
      },
    ];
    let packages = vec![
      "com.discord.permission.CONNECT",
      "com.discord.file-provider",
      "com.discord.firebaseinitprovider",
      "com.discord.workmanager-init",
    ];

    for package in packages {
      replacements.push(Replacement {
        from: package.to_string(),
        to: base_package.to_string() + &package["com.discord".len()..],
      });
    }

    loop {
      let chunk_start = reader.position();
      if chunk_start == reader.get_ref().len() as u64 {
        panic!("Found end of data");
      }
      let mut chunk = ResChunkHeader {
        r#type: reader.read_u16::<LittleEndian>()?,
        header_size: reader.read_u16::<LittleEndian>()?,
        size: reader.read_u32::<LittleEndian>()?,
      };
      let original_chunk_size = chunk.size;
      debug!("Found chunk of type {}", chunk.r#type);
      if chunk.r#type == RES_STRING_POOL_TYPE {
        debug!("Found string pool at {}!", reader.position());
        let mut pool_header = StringPoolHeader {
          string_count: reader.read_u32::<LittleEndian>()?,
          style_count: reader.read_u32::<LittleEndian>()?,
          flags: reader.read_u32::<LittleEndian>()?,
          strings_start: reader.read_u32::<LittleEndian>()?,
          styles_start: reader.read_u32::<LittleEndian>()?,
        };
        assert!(chunk.header_size == 28);

        reader.set_position(chunk_start + (chunk.header_size as u64));
        debug!("At position: {}", chunk_start + (chunk.header_size as u64));

        let utf8 = pool_header.flags & UTF8_FLAG == UTF8_FLAG;
        let index_position = chunk.header_size as u64;

        let string_position = pool_header.strings_start as u64;
        debug!("There are {} strings", pool_header.string_count);
        for index in 0..pool_header.string_count {
          let off_position = index_position + (index as u64) * (size_of::<u32>() as u64);
          debug!(
            "This string ({})'s offset lives at {}",
            index,
            chunk_start + off_position
          );
          let character_size: u64 = match utf8 {
            true => size_of::<u8>() as u64,
            false => size_of::<u16>() as u64,
          };
          reader.set_position(chunk_start + off_position);
          let off = reader.read_u32::<LittleEndian>()? as u64;
          if !utf8 {
            debug!("Off: {}", off);
            let str_base = string_position + off;
            reader.set_position(chunk_start + (str_base as u64));
            debug!(
              "We just seeked to {} for string data",
              chunk_start + (str_base as u64)
            );

            let mut length = reader.read_u16::<LittleEndian>()? as u32;
            if length & 0x8000 != 0 {
              length = ((length & 0x7FFF) << 16) | (reader.read_u16::<LittleEndian>()? as u32);
            }
            debug!(
              "Length: {}. Styles start: {}",
              length, pool_header.styles_start
            );

            let mut utf16_string = Vec::with_capacity(length as usize);
            for _ in 0..length {
              utf16_string.push(reader.read_u16::<LittleEndian>()?);
            }
            let mut string_value = String::from_utf16(&utf16_string)?;
            for replacement in &replacements {
              if replacement.from == string_value {
                debug!("Replacing string at index {}!", index);
                reader.set_position(chunk_start + off_position + size_of::<u32>() as u64);
                let delta =
                  (replacement.to.len() - replacement.from.len()) * character_size as usize;
                for index in index + 1..pool_header.string_count {
                  debug!("Updating offset for string at index {}", index);
                  // Scuffed
                  let old_position = reader.position();
                  let value = reader.read_u32::<LittleEndian>()?;
                  reader.set_position(old_position);
                  debug!(
                    "Writing {} at {} (was {})",
                    value + delta as u32,
                    old_position,
                    value
                  );
                  reader.write_u32::<LittleEndian>(value + delta as u32)?;
                }

                if pool_header.style_count != 0 {
                  pool_header.styles_start += delta as u32;
                }
                chunk.size += delta as u32;

                reader.set_position(chunk_start + (str_base as u64));
                assert!(replacement.to.len() < 0x7fff);
                let mut old_length = length;
                if length > 0x7fff {
                  old_length += 1;
                }
                reader.write_u16::<LittleEndian>(replacement.to.len() as u16)?;
                length = replacement.to.len() as u32;
                string_value = replacement.to.clone();
                let mut ctr = 0;
                for chunk in replacement.to.encode_utf16() {
                  if ctr >= old_length {
                    debug!("Inserting!");
                    // Dumb hack, insert the next character's bytes
                    let position = reader.position() as usize;
                    reader.get_mut().insert(position, (chunk & 0xff) as u8);
                    reader.get_mut().insert(position + 1, (chunk >> 8) as u8);
                    reader.set_position(position as u64 + character_size);
                  } else {
                    reader.write_u16::<LittleEndian>(chunk)?;
                  }
                  ctr += 1;
                }
              }
            }

            debug!("Read: {}", string_value);
            debug!("Null byte should be at {}", reader.position());
            let last_character = reader.read_u16::<LittleEndian>()?;
            // Debugging
            if last_character != 0 {
              debug!("Got stuck :(");
              break;
            }
            assert!(last_character == 0);
          }
        }

        let padding_needed = (4 - (chunk.size & 3)) & 3;
        debug!(
          "Ready to finalize string pool of size {}. We need to add {} bytes of padding.",
          chunk.size, padding_needed
        );
        for _ in 0..padding_needed {
          reader
            .get_mut()
            .insert((chunk_start + chunk.size as u64) as usize, 0);
        }
        chunk.size += padding_needed;
        assert!((chunk.size & 0x3) == 0);

        reader.set_position(chunk_start);
        reader.write_u16::<LittleEndian>(chunk.r#type)?;
        reader.write_u16::<LittleEndian>(chunk.header_size)?;
        reader.write_u32::<LittleEndian>(chunk.size)?;
        reader.write_u32::<LittleEndian>(pool_header.string_count)?;
        reader.write_u32::<LittleEndian>(pool_header.style_count)?;
        reader.write_u32::<LittleEndian>(pool_header.flags)?;
        reader.write_u32::<LittleEndian>(pool_header.strings_start)?;
        reader.write_u32::<LittleEndian>(pool_header.styles_start)?;
        assert!(reader.position() == chunk_start + chunk.header_size as u64);

        // MASSIVE CHEESE.
        reader.set_position(0);
        reader.read_u16::<LittleEndian>()?;
        reader.read_u16::<LittleEndian>()?;
        let parent_size_pos = reader.position();
        let current_parent_size = reader.read_u32::<LittleEndian>()?;
        reader.set_position(parent_size_pos);
        // ^^ Type and header_size
        let size_delta = chunk.size - original_chunk_size;
        debug!("Updating parent node, size has grown by: {}", size_delta);
        reader.write_u32::<LittleEndian>(current_parent_size + size_delta)?;
        reader.set_position(chunk_start + (chunk.size as u64));
        break;
      } else if chunk.r#type == RES_XML_TYPE {
        // RES_XML_TYPE just contains a flat array of nodes. Let's iterate into them...
        reader.set_position(chunk_start + (chunk.header_size as u64));
      } else {
        reader.set_position(chunk_start + (chunk.size as u64));
      }
    }
    Ok(reader.into_inner())
  }

  pub fn update_arsc(
    arsc: Vec<u8>,
    package_old: String,
    package_new: String,
  ) -> Result<Vec<u8>, Box<dyn Error + Send + Sync>> {
    let mut reader = Cursor::new(arsc);

    loop {
      let chunk_start = reader.position();
      if chunk_start == reader.get_ref().len() as u64 {
        // panic!("Found end of data");
        break;
      }
      let chunk = ResChunkHeader {
        r#type: reader.read_u16::<LittleEndian>()?,
        header_size: reader.read_u16::<LittleEndian>()?,
        size: reader.read_u32::<LittleEndian>()?,
      };
      debug!("Read chunk of type {}", chunk.r#type);
      if chunk.r#type == RES_TABLE_TYPE {
        let package_count = reader.read_u32::<LittleEndian>()?;
        debug!("Found table of {} packages!", package_count);
      } else if chunk.r#type == RES_STRING_POOL_TYPE {
        debug!("Found the string pool. Skipping it!");
        reader.set_position(chunk_start + chunk.size as u64);
      } else if chunk.r#type == RES_TABLE_PACKAGE_TYPE {
        debug!("Found the package table");
        let custom_start = reader.position();
        let package = ResTablePackage {
          id: reader.read_u32::<LittleEndian>()?,
          name: {
            let mut name = [0u16; 128];
            for i in 0..name.len() {
              name[i] = reader.read_u16::<LittleEndian>()?;
            }
            name
          },
          type_strings: reader.read_u32::<LittleEndian>()?,
          last_public_type: reader.read_u32::<LittleEndian>()?,
          key_strings: reader.read_u32::<LittleEndian>()?,
          last_public_key: reader.read_u32::<LittleEndian>()?,
          type_id_offset: reader.read_u32::<LittleEndian>()?,
        };

        let null_index = package
          .name
          .iter()
          .position(|&entry| entry == 0u16)
          .ok_or(AXMLError::MissingNullByte)?;
        let name_str = String::from_utf16(&package.name[..null_index])?;
        if name_str == package_old {
          debug!("Replacing package");
          reader.set_position(custom_start);
          // Skip past id
          reader.read_u32::<LittleEndian>()?;
          // Write new name
          for character in package_new.encode_utf16() {
            reader.write_u16::<LittleEndian>(character)?;
          }
          // Must be terminated with NUL
          reader.write_u16::<LittleEndian>(0)?;
        }

        debug!("Read package {}! Skipping past it.", name_str);
        reader.set_position(chunk_start + chunk.size as u64);
      } else {
        error!("Found unknown data chunk: {}", chunk.r#type);
        panic!("Found unknown data chunk: {}", chunk.r#type);
      }
    }

    Ok(reader.into_inner())
  }
}
