#![allow(non_camel_case_types)]

use std::os::raw::{c_char, c_void};

pub type elf_binary_t = c_void;

#[link(name = "LIEF")]
extern "C" {
  pub fn LIEF_ELF_Parser_parse(
    data: *const u8,
    length: usize,
    filename: *const c_char,
  ) -> *mut elf_binary_t;
  pub fn LIEF_ELF_Binary_add_library(binary: *mut elf_binary_t, filename: *const c_char);
  pub fn LIEF_ELF_Binary_raw(binary: *mut elf_binary_t, length: *mut usize) -> *mut u8;
  pub fn LIEF_ELF_Binary_delete(binary: *mut elf_binary_t);
  pub fn LIEF_free(ptr: *mut c_void);
}
