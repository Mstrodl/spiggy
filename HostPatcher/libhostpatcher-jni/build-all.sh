# cd into script dir
cd -- "$( dirname -- "${BASH_SOURCE[0]}" )"
cd ..

for triple in aarch64-linux-android \
              armv7-linux-androideabi \
              i686-linux-android \
              x86_64-linux-android; do
  cross build --release --target $triple
done
