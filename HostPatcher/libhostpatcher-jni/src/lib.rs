#[macro_use]
extern crate log;
#[cfg(target_os = "android")]
extern crate android_log;
#[cfg(not(target_os = "android"))]
extern crate env_logger;

use jni::objects::{JClass, JString};
use jni::JNIEnv;
use libhostpatcher::APK;
use std::fs::File;

#[cfg(target_os = "android")]
fn init() {
  let _ = android_log::init("Spiggy");
}

#[cfg(not(target_os = "android"))]
fn init() {
  let _ = env_logger::try_init();
}

#[allow(non_snake_case)]
#[no_mangle]
pub extern "system" fn Java_tech_coolmathgames_spiggy_patcher_NativePatcher_patch(
  env: JNIEnv,
  _class: JClass,
  original_file: JString,
  new_file: JString,
  package_name: JString,
  display_name: JString,
  icon: JString,
  preload_path: JString,
  cache_dir: JString,
  architectures: JString,
) {
  init();
  debug!("Booted libhostpatcher-jni");
  // return env.byte_array_from_slice(&vec![]).unwrap();

  let package_name = env.get_string(package_name).unwrap();
  let package_name = package_name.to_str().unwrap();
  let display_name = env.get_string(display_name).unwrap();
  let display_name = display_name.to_str().unwrap();
  let preload_path = env.get_string(preload_path);
  let preload_path = match &preload_path {
    Ok(jstr) => Some(jstr.to_str().unwrap()),
    Err(_) => None,
  };
  let cache_dir = env.get_string(cache_dir).unwrap();
  let cache_dir = cache_dir.to_str().unwrap();
  debug!("Grabbed main properties");
  let icon = env.get_string(icon);
  let icon = match &icon {
    Ok(jstr) => Some(jstr.to_str().unwrap()),
    Err(_) => None,
  };
  debug!("Got icon?");

  let architectures = env.get_string(architectures).unwrap();
  let architectures = architectures.to_str();
  let architectures: Option<Vec<String>> = architectures
    .map(|architectures| {
      architectures
        .split("/")
        .map(|value| value.to_string())
        .collect()
    })
    .ok();

  let original_file = env.get_string(original_file).unwrap();
  let original_file = original_file.to_str().unwrap();
  let new_file = env.get_string(new_file).unwrap();
  let new_file = new_file.to_str().unwrap();
  debug!("Read new files");

  let original_file = File::open(original_file).unwrap();
  let new_file = File::create(new_file).unwrap();
  debug!("Opened handles");
  let mut apk = APK::new(original_file, new_file).unwrap();
  debug!("Created APK");
  apk
    .patch(
      package_name,
      display_name,
      icon.map(|file| std::fs::read(file).unwrap()),
      preload_path,
      Some(cache_dir.to_string()),
      architectures,
    )
    .unwrap();
  debug!("Patched!");
}
