package tech.coolmathgames.spiggy.patcher;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import java.util.Objects;

import tech.coolmathgames.spiggy.patcher.databinding.FragmentAppCreateBinding;

public class AppCreateFragment extends Fragment {

  private FragmentAppCreateBinding binding;

  @Override
  public View onCreateView(
    @NonNull LayoutInflater inflater, ViewGroup container,
    Bundle savedInstanceState
  ) {

    binding = FragmentAppCreateBinding.inflate(inflater, container, false);
    return binding.getRoot();

  }

  public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    binding.packageName.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override
      public void afterTextChanged(Editable s) {
        binding.packageName.setError(null);
      }
    });
    binding.branchName.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override
      public void afterTextChanged(Editable s) {
        binding.branchName.setError(null);
      }
    });
    binding.displayName.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override
      public void afterTextChanged(Editable s) {
        binding.displayName.setError(null);
      }
    });
    binding.displayName.setError(null);
    binding.branchName.setError(null);
    binding.packageName.setError(null);

    binding.buttonSecond.setOnClickListener(view1 -> new Thread(() -> {
      SpiggyApp spiggyApp = (SpiggyApp) AppCreateFragment.this.requireActivity().getApplication();
      StorageController sc = spiggyApp.getStorageController();
      String packageName = binding.packageName.getText().toString();
      // NOTE: We can't check for conflicts with other installed apps we don't manage
      // which sucks, but the error message is okay-enough ("Update") that I'm not very worried
      for (PatchedApp app : sc.getApps()) {
        if (app.getPackageName().equals(packageName)) {
          this.requireActivity().runOnUiThread(() -> binding.packageName.setError(this.getString(R.string.package_name_unique)));
          return;
        }
      }
      if (!binding.branchName.getText().toString().equals(this.getString(R.string.default_branch))) {
        this.requireActivity().runOnUiThread(() -> binding.branchName.setError(this.getString(R.string.invalid_branch_name)));
        return;
      }
      Objects.requireNonNull(spiggyApp.getActivity()).doPatch(() -> {
        PatchedApp app = sc.createApp(
          binding.displayName.getText().toString(),
          packageName,
          binding.branchName.getText().toString(),
          false
        );
        sc.updateBranch(app.getBranch());
        app.patch();
        this.requireActivity().runOnUiThread(() -> NavHostFragment.findNavController(AppCreateFragment.this)
          .navigate(R.id.action_app_created));
      });
    }).start());
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    binding = null;
  }

}