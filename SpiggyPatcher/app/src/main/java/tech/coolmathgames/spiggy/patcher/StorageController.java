package tech.coolmathgames.spiggy.patcher;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyProperties;

import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.security.auth.x500.X500Principal;

public class StorageController {
  public static final int UPDATE_BRANCHES = 3000;
  private final SpiggyApp spiggyApp;
  private final DownloadManager downloadManager;
  private final SharedPreferences prefs;
  private final Gson gson;
  private List<PatchedApp> apps;

  public StorageController(SpiggyApp spiggyApp) {
    this.spiggyApp = spiggyApp;
    this.prefs = spiggyApp.getSharedPreferences("preferences", Context.MODE_PRIVATE);
    GsonBuilder builder = new GsonBuilder();
    this.gson = builder.create();
    this.downloadManager = (DownloadManager) spiggyApp.getSystemService(Context.DOWNLOAD_SERVICE);
  }

  @Nullable
  public ApplicationInfo getDiscordAppInfo() {
    @SuppressLint("QueryPermissionsNeeded") List<ApplicationInfo> packages = this.getSpiggyApp()
      .getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);

    for (ApplicationInfo applicationInfo : packages) {
      if (applicationInfo.packageName.equals("com.discord")) {
        return applicationInfo;
      }
    }
    return null;
  }

  public PatchedApp createApp(String displayName, String packageName, String branch, boolean hasIcon) {
    PatchedAppData data = new PatchedAppData(displayName, packageName, branch, hasIcon);
    PatchedApp app = new PatchedApp(data, this);
    this.getApps().add(app);
    this.syncApps();
    return app;
  }

  @WorkerThread
  public List<PatchedApp> getApps() {
    if (this.apps == null) {
      System.out.println("Parsing app list, this is slow!");
      String apps = this.prefs.getString("apps", "[]");
      Type listType = new TypeToken<List<PatchedAppData>>() {
      }.getType();
      List<PatchedAppData> appsList = gson.fromJson(apps, listType);
      this.apps = new ArrayList<>();
      for (PatchedAppData data : appsList) {
        PatchedApp patchedApp = new PatchedApp(data, this);
        this.apps.add(patchedApp);
      }
    }
    return this.apps;
  }

  public File getIconFolder() {
    File dataDir = this.getSpiggyApp().getFilesDir();
    return new File(dataDir, "icons");
  }

  @WorkerThread
  public void syncApps() {
    SharedPreferences.Editor editor = this.prefs.edit();
    List<PatchedAppData> appDataList = new ArrayList<>();
    for (PatchedApp app : apps) {
      appDataList.add(app.getData());
    }
    editor.putString("apps", gson.toJson(appDataList));
    editor.apply();
    Objects.requireNonNull(this.spiggyApp.getActivity()).syncApps();
  }

  public SpiggyApp getSpiggyApp() {
    return this.spiggyApp;
  }

  // Okay, now this is epic: https://stackoverflow.com/a/27869798
  public KeyStore.PrivateKeyEntry getSigningKey()
    throws NoSuchProviderException, NoSuchAlgorithmException, KeyStoreException, CertificateException,
    IOException, InvalidAlgorithmParameterException, UnrecoverableEntryException {
    KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
    keyStore.load(null);

    String alias = "signingKey"; // replace as required or get it as a function argument

    // Create the keys if necessary
    if (!keyStore.containsAlias(alias)) {
      Calendar notBefore = Calendar.getInstance();
      Calendar notAfter = Calendar.getInstance();
      notAfter.add(Calendar.YEAR, 100);
      String algorithm;
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        algorithm = KeyProperties.KEY_ALGORITHM_RSA;
      } else {
        algorithm = "RSA";
      }
      @SuppressLint("WrongConstant") KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(this.spiggyApp)
        .setAlias(alias)
        .setKeyType(algorithm)
        .setKeySize(4096)
        .setSubject(new X500Principal("CN=tech.coolmathgames.spiggy"))
        .setSerialNumber(BigInteger.valueOf(System.currentTimeMillis()))
        .setStartDate(notBefore.getTime())
        .setEndDate(notAfter.getTime())
        .build();
      KeyPairGenerator generator = KeyPairGenerator.getInstance(algorithm, "AndroidKeyStore");
      generator.initialize(spec);

      KeyPair keyPair = generator.generateKeyPair();
    }

    // Retrieve the keys
    return (KeyStore.PrivateKeyEntry) keyStore.getEntry(alias, null);
  }

  public void updateBranches() {
    if (!this.ensureWritable()) {
      return;
    }
    Set<String> branches = new HashSet<>();
    for (PatchedApp app : this.getApps()) {
      branches.add(app.getBranch());
    }
    // We don't *really* care about the result of this
    for (String branch : branches) {
      updateBranch(branch);
    }
  }

  public boolean ensureWritable() {
    if (
      Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
        Build.VERSION.SDK_INT < Build.VERSION_CODES.Q &&
        spiggyApp.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
    ) {
      Objects.requireNonNull(spiggyApp.getActivity())
        .requestPermissions(new String[]{
          Manifest.permission.WRITE_EXTERNAL_STORAGE
        }, StorageController.UPDATE_BRANCHES);
      return false;
    }
    return true;
  }

  public void updateBranch(String branch) {
    // TODO: Must grab permissions on >= M && < Q
    if (!this.ensureWritable()) {
      return;
    }
    Uri uri = Uri.parse("https://gitlab.com/api/v4/projects/" + SpiggyApp.PROJECT_NAME + "/jobs/artifacts/" + branch + "/raw/dist/preload.js?job=preload");
    String path = "spiggy/" + branch + ".js";
    // TODO: The download could be queued. Ideally we should download to temp dir and move into place afterwards
    File newPath = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), path);
    //noinspection ResultOfMethodCallIgnored
    newPath.delete();
    DownloadManager.Request request = new DownloadManager.Request(uri)
      .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI |
        DownloadManager.Request.NETWORK_MOBILE)
      .setAllowedOverRoaming(true)
      .setAllowedOverMetered(true)
      .setTitle("Preload for " + branch)
      .setDescription("Downloading preload for " + branch)
      .setDestinationInExternalPublicDir(
        Environment.DIRECTORY_DOWNLOADS,
        path
      );
    this.downloadManager.enqueue(request);
  }
}
