package tech.coolmathgames.spiggy.patcher;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import tech.coolmathgames.spiggy.patcher.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

  private static final int APP_INSTALLED_BASE = 6900;
  private AppBarConfiguration appBarConfiguration;
  private ActivityMainBinding binding;
  private String requestingPackageName;
  private boolean working = false;
  private ArrayList<String> installing = new ArrayList<>();
  private final ActivityResultLauncher<String> getContent = registerForActivityResult(new ActivityResultContracts.GetContent(), result -> {
    if (result == null) return;
    new Thread(() ->
      this.doPatch(() -> {
        Bitmap bitmap;
        try {
          bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), result);
        } catch (IOException e) {
          e.printStackTrace();
          return;
        }
        SpiggyApp spiggyApp = (SpiggyApp) getApplication();
        StorageController sc = spiggyApp.getStorageController();
        for (PatchedApp patchedApp : sc.getApps()) {
          if (patchedApp.getPackageName().equals(requestingPackageName)) {
            try {
              patchedApp.setIcon(bitmap);
              patchedApp.patch();
            } catch (IOException e) {
              e.printStackTrace();
            }
            break;
          }
        }
      })
    ).start();
  });
  private AppListFragment currentFragment;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    SpiggyApp app = (SpiggyApp) this.getApplication();
    app.setActivity(this);

    // if (app.getStorageController().getDiscordAppInfo() == null) {
    //   Toast.makeText(this, R.string.discord_not_installed, Toast.LENGTH_LONG).show();
    //   this.finish();
    //   return;
    // }

    binding = ActivityMainBinding.inflate(getLayoutInflater());
    setContentView(binding.getRoot());

    setSupportActionBar(binding.toolbar);

    NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
    appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
    NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
  }

  public boolean doPatch(Runnable execute) {
    if (this.isWorking()) {
      this.runOnUiThread(() ->
        Toast.makeText(this, R.string.already_patching, Toast.LENGTH_SHORT)
          .show()
      );
      return false;
    }
    this.working = true;
    this.runOnUiThread(() -> this.binding.progressBar.setVisibility(View.VISIBLE));
    execute.run();
    this.runOnUiThread(() -> this.binding.progressBar.setVisibility(View.GONE));
    this.working = false;
    return true;
  }

  @Override
  public boolean onCreateOptionsMenu(@NonNull Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  public boolean onSupportNavigateUp() {
    NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
    return NavigationUI.navigateUp(navController, appBarConfiguration)
      || super.onSupportNavigateUp();
  }

  @Override
  public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
    super.onSaveInstanceState(savedInstanceState);
    savedInstanceState.putString("requestingPackageName", this.requestingPackageName);
    savedInstanceState.putStringArrayList("installing", this.installing);
  }

  @Override
  protected void onRestoreInstanceState(Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);
    this.requestingPackageName = savedInstanceState.getString("requestingPackageName");
    this.installing = savedInstanceState.getStringArrayList("installing");
  }

  public void selectImage(PatchedApp patchedApp) {
    this.requestingPackageName = patchedApp.getPackageName();
    this.getContent.launch("image/*");
  }

  public void syncApps() {
    Fragment fragmentMain = this.getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment_content_main);
    if (fragmentMain != null) {
      FragmentManager manager = fragmentMain.getChildFragmentManager();
      for (Fragment fragment : manager.getFragments()) {
        if (fragment instanceof RefreshableFragment) {
          ((RefreshableFragment) fragment).refreshApps();
        }
      }
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    int location = requestCode - APP_INSTALLED_BASE;
    if (location < this.installing.size()) {
      String path = this.installing.get(location);
      if (path != null) {
        this.installing.set(location, null);
        for (int i = this.installing.size() - 1; i >= 0; --i) {
          if (this.installing.get(i) == null) {
            this.installing.remove(i);
          } else {
            break;
          }
        }
        File oldAPK = new File(path);
        //noinspection ResultOfMethodCallIgnored
        oldAPK.delete();
        String packageName = oldAPK.getName().substring(0, oldAPK.getName().length() - 4);
        if (resultCode == RESULT_OK) {
          SpiggyApp spiggyApp = (SpiggyApp) this.getApplication();
          StorageController sc = spiggyApp.getStorageController();
          for (PatchedApp patchedApp : sc.getApps()) {
            if (patchedApp.getPackageName().equals(packageName)) {
              if (!patchedApp.getInstalled()) {
                patchedApp.setInstalled(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                  Bundle args = new Bundle();
                  args.putString(PermissionEducation.PACKAGE_NAME, patchedApp.getPackageName());
                  NavHostFragment.findNavController(this.currentFragment)
                    .navigate(R.id.action_app_installed, args);
                }
              }
            }
          }
        }
      }
    }
  }

  public int addRequest(String path) {
    int i;
    for (i = 0; i < this.installing.size(); ++i) {
      if (this.installing.get(i) == null) {
        break;
      }
    }
    if (i == this.installing.size()) {
      this.installing.add(path);
    } else {
      this.installing.set(i, path);
    }
    return APP_INSTALLED_BASE + i;
  }

  public boolean isWorking() {
    return working;
  }

  public void setCurrentFragment(AppListFragment currentFragment) {
    this.currentFragment = currentFragment;
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (requestCode == StorageController.UPDATE_BRANCHES) {
      SpiggyApp app = (SpiggyApp) this.getApplication();
      app.getStorageController().updateBranches();
    }
  }
}