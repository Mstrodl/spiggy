package tech.coolmathgames.spiggy.patcher;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import tech.coolmathgames.spiggy.patcher.databinding.FragmentPermissionEducationBinding;

public class PermissionEducation extends Fragment {
  public static final String PACKAGE_NAME = "tech.coolmathgames.spiggy.patcher.PermissionEducation";
  private FragmentPermissionEducationBinding binding;

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater,
                           @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {

    binding = FragmentPermissionEducationBinding.inflate(inflater, container, false);
    return binding.getRoot();
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    binding.skipButton.setOnClickListener(view1 ->
      NavHostFragment.findNavController(this)
        .navigate(R.id.action_permissions_finalized)
    );
    binding.gotoSettings.setOnClickListener(view1 -> {
      assert this.getArguments() != null;
      Uri pkg = Uri.parse("package:" + this.getArguments().getString(PACKAGE_NAME));
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
        this.requireActivity().startActivity(
          new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION)
            .setData(pkg)
        );
      } else {
        this.requireActivity().startActivity(
          new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            .addCategory(Intent.CATEGORY_DEFAULT)
            .setData(pkg)
        );
      }
      NavHostFragment.findNavController(this)
        .navigate(R.id.action_permissions_finalized);
    });
    this.hide();
  }

  @Override
  public void onResume() {
    super.onResume();
    this.hide();
  }

  @Override
  public void onPause() {
    super.onPause();
    this.show();
  }

  @Override
  public void onDestroy() {
    this.show();
    super.onDestroy();
  }

  private void hide() {
    // Hide UI first
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.hide();
    }
  }

  @SuppressLint("InlinedApi")
  private void show() {
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.show();
    }
  }

  @Nullable
  private ActionBar getSupportActionBar() {
    ActionBar actionBar = null;
    if (getActivity() instanceof AppCompatActivity) {
      AppCompatActivity activity = (AppCompatActivity) getActivity();
      actionBar = activity.getSupportActionBar();
    }
    return actionBar;
  }

  @Nullable
  private MainActivity getMainActivity() {
    if (this.getActivity() != null) {
      SpiggyApp spiggyApp = (SpiggyApp) this.getActivity().getApplication();
      return spiggyApp.getActivity();
    }
    return null;
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    binding = null;
  }
}