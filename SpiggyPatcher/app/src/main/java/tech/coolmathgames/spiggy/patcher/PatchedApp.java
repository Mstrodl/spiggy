package tech.coolmathgames.spiggy.patcher;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.core.content.FileProvider;

import com.android.apksig.ApkSigner;
import com.android.apksig.util.DataSources;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PatchedApp {
  private final PatchedAppData data;
  private final StorageController storageController;

  public PatchedApp(PatchedAppData data, StorageController storageController) {
    this.data = data;
    this.storageController = storageController;
  }

  public static boolean ensureInstallable(@NonNull Activity activity) {
    Application app = activity.getApplication();
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O &&
      !app.getPackageManager().canRequestPackageInstalls()) {
      activity.startActivity(
        new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES)
          .setData(Uri.parse(String.format("package:%s", app.getPackageName())))
      );
      return false;
    }
    return true;
  }

  public String getPackageName() {
    return this.data.getPackageName();
  }

  public String getDisplayName() {
    return this.data.getDisplayName();
  }

  public String getBranch() {
    return this.data.getBranch();
  }

  public void setBranch(String name) {
    this.data.setBranch(name);
    this.sync();
  }

  public void setIcon(@Nullable Bitmap bitmap) throws IOException {
    //noinspection ResultOfMethodCallIgnored
    this.storageController.getIconFolder().mkdirs();
    File outputPath = this.getIconPath();
    if (bitmap != null) {
      OutputStream output = new FileOutputStream(outputPath);
      if (bitmap.getHeight() != bitmap.getWidth()) {
        int size = Math.min(bitmap.getHeight(), bitmap.getWidth());
        bitmap = Bitmap.createBitmap(
          bitmap,
          (bitmap.getWidth() - size) / 2,
          (bitmap.getHeight() - size) / 2,
          size,
          size
        );
      }
      bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
      output.close();
    } else {
      //noinspection ResultOfMethodCallIgnored
      outputPath.delete();
    }
    this.data.setHasIcon(bitmap != null);
    this.sync();
  }

  public File getIconPath() {
    return new File(this.storageController.getIconFolder(), this.getPackageName() + ".png");
  }

  public PatchedAppData getData() {
    return this.data;
  }

  private void sync() {
    this.storageController.syncApps();
  }

  @WorkerThread
  public void patch() {
    SpiggyApp spiggyApp = this.storageController.getSpiggyApp();
    if (!PatchedApp.ensureInstallable(Objects.requireNonNull(spiggyApp.getActivity()))) {
      return;
    }
    ApplicationInfo applicationInfo = this.storageController.getDiscordAppInfo();
    if (applicationInfo == null) {
      Toast.makeText(spiggyApp, R.string.discord_not_installed, Toast.LENGTH_LONG).show();
      return;
    }

    String iconPath = null;
    if (this.getHasIcon()) {
      iconPath = this.getIconPath().getAbsolutePath();
    }

    File outputDir = spiggyApp.getCacheDir();
    File signedDir;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      signedDir = new File(outputDir, "signed");
    } else {
      // Some permissions weirdness is here
      signedDir = new File(spiggyApp.getExternalCacheDir(), "signed");
    }
    //noinspection ResultOfMethodCallIgnored
    signedDir.mkdirs();
    File tempSignedAPK;
    tempSignedAPK = new File(signedDir, this.getPackageName() + ".apk");
    File tempAPK;
    tempAPK = new File(outputDir, this.getPackageName() + ".apk");

    File preloadPath = new File(
      Environment.getExternalStoragePublicDirectory(
        Environment.DIRECTORY_DOWNLOADS
      ),
      "spiggy/" + this.getBranch() + ".js"
    );

    StringBuilder abis = new StringBuilder();
    for (String abi : Build.SUPPORTED_ABIS) {
      if (abis.length() != 0) {
        abis.append("/");
      }
      abis.append(abi);
    }

    NativePatcher.patch(
      Objects.requireNonNull(applicationInfo.publicSourceDir),
      tempAPK.getAbsolutePath(),
      Objects.requireNonNull(this.getPackageName()),
      Objects.requireNonNull(this.getDisplayName()),
      iconPath,
      preloadPath.getAbsolutePath(),
      new File(spiggyApp.getCacheDir(), "gadget").getAbsolutePath(),
      // Cheaper and easier than messing with unsafe Rust:
      abis.toString()
    );
    System.out.println("Patched APK!");

    KeyStore.PrivateKeyEntry privateKeyEntry;
    try {
      privateKeyEntry = storageController.getSigningKey();
    } catch (Exception e) {
      e.printStackTrace();
      return;
    }
    PrivateKey privateKey = privateKeyEntry.getPrivateKey();
    X509Certificate certificate = (X509Certificate) privateKeyEntry.getCertificate();

    List<X509Certificate> certificates = new ArrayList<>(1);
    certificates.add(certificate);

    ApkSigner.SignerConfig signerConfig = new ApkSigner.SignerConfig.Builder(
      "spiggy", privateKey, certificates
    ).build();
    List<ApkSigner.SignerConfig> signerConfigs = new ArrayList<>(1);
    signerConfigs.add(signerConfig);

    System.out.println("Ready to sign: " + tempAPK.getAbsolutePath());
    ApkSigner signer;
    try {
      signer = new ApkSigner.Builder(signerConfigs)
        .setInputApk(DataSources.asDataSource(new RandomAccessFile(tempAPK, "r")))
        .setOutputApk(tempSignedAPK)
        .build();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      return;
    }
    try {
      signer.sign();
    } catch (Exception e) {
      e.printStackTrace();
      return;
    }
    //noinspection ResultOfMethodCallIgnored
    tempAPK.delete();

    Uri signedAPKURI;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      signedAPKURI = FileProvider.getUriForFile(
        spiggyApp,
        BuildConfig.APPLICATION_ID + ".fileprovider",
        tempSignedAPK
      );
    } else {
      signedAPKURI = Uri.fromFile(tempSignedAPK);
    }

    installAPK(signedAPKURI, tempSignedAPK);

    System.out.println("We installed!");
  }

  private void installAPK(Uri signedAPKURI, File apkFile) {
    SpiggyApp spiggyApp = this.storageController.getSpiggyApp();
    Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE)
      .setData(signedAPKURI)
      .putExtra(Intent.EXTRA_RETURN_RESULT, true)
      .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
    assert spiggyApp.getActivity() != null;
    int id = spiggyApp.getActivity().addRequest(apkFile.getAbsolutePath());
    spiggyApp.getActivity().startActivityForResult(intent, id);
  }

  public boolean getHasIcon() {
    return this.data.getHasIcon();
  }

  public void delete() {
    new Thread(() -> {
      this.storageController.getApps().remove(this);
      this.storageController.syncApps();
      //noinspection ResultOfMethodCallIgnored
      this.getIconPath().delete();
    }).start();
  }

  public boolean getInstalled() {
    return this.data.getInstalled();
  }

  public void setInstalled(boolean installed) {
    this.data.setInstalled(installed);
    this.sync();
  }
}
