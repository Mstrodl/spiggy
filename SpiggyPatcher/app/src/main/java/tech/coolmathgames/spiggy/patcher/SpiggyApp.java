package tech.coolmathgames.spiggy.patcher;

import android.app.Application;

import androidx.annotation.Nullable;

public class SpiggyApp extends Application {
  @SuppressWarnings("SpellCheckingInspection")
  public static final String PROJECT_NAME = "mstrodl%2Fspiggy";
  private StorageController storageController;
  private MainActivity activity;

  public SpiggyApp() {
    super();
  }

  public void onCreate() {
    super.onCreate();
    this.storageController = new StorageController(this);
  }

  public StorageController getStorageController() {
    return storageController;
  }

  @Nullable
  public MainActivity getActivity() {
    return activity;
  }

  public void setActivity(@Nullable MainActivity activity) {
    this.activity = activity;
  }
}
