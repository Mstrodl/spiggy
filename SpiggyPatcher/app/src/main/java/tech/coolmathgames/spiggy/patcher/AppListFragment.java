package tech.coolmathgames.spiggy.patcher;

import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import java.util.List;
import java.util.Objects;

import tech.coolmathgames.spiggy.patcher.databinding.FragmentAppListBinding;

public class AppListFragment extends Fragment implements RefreshableFragment {

  private static final ColorDrawable DISCORD_BACKGROUND = new ColorDrawable(0xfffaa61a);
  private FragmentAppListBinding binding;

  @Override
  public View onCreateView(
    @NonNull LayoutInflater inflater, ViewGroup container,
    Bundle savedInstanceState
  ) {
    SpiggyApp spiggyApp = (SpiggyApp) this.requireActivity().getApplication();
    Objects.requireNonNull(spiggyApp.getActivity()).setCurrentFragment(this);
    binding = FragmentAppListBinding.inflate(inflater, container, false);
    return binding.getRoot();

  }

  public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    binding.buttonCreate.setOnClickListener(view13 -> NavHostFragment.findNavController(this)
      .navigate(R.id.action_create_app));

    SpiggyApp spiggyApp = (SpiggyApp) this.requireActivity().getApplication();
    binding.fab.setOnClickListener(view12 -> {
      if (!PatchedApp.ensureInstallable(this.requireActivity())) {
        return;
      }
      new Thread(() ->
        Objects.requireNonNull(spiggyApp.getActivity()).doPatch(() -> {
          StorageController sc = spiggyApp.getStorageController();
          sc.updateBranches();
          for (PatchedApp patchedApp : sc.getApps()) {
            patchedApp.patch();
          }
        })
      ).start();
    });

    this.inflateAppList();
  }

  public void refreshApps() {
    this.inflateAppList();
  }

  private void inflateAppList() {
    System.out.println("Inflating app list!");
    SpiggyApp spiggyApp = (SpiggyApp) this.requireActivity().getApplication();
    List<PatchedApp> apps = spiggyApp.getStorageController().getApps();
    this.requireActivity().runOnUiThread(() -> {
      binding.appList.removeAllViews();
      for (PatchedApp patchedApp : apps) {
        View appButton = View.inflate(this.getContext(), R.layout.app_button, null);
        binding.appList.addView(appButton);
        TextView appName = appButton.findViewById(R.id.appName);
        appName.setText(patchedApp.getPackageName());
        appButton.setOnClickListener(view1 -> new Thread(() ->
            Objects.requireNonNull(spiggyApp.getActivity()).doPatch(() -> {
              spiggyApp.getStorageController().updateBranch(patchedApp.getBranch());
              patchedApp.patch();
            })
          ).start()
        );
        ImageView appIcon = appButton.findViewById(R.id.appIcon);
        if (patchedApp.getHasIcon()) {
          Uri uri;
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            uri = FileProvider.getUriForFile(
              spiggyApp,
              BuildConfig.APPLICATION_ID + ".fileprovider",
              patchedApp.getIconPath()
            );
          } else {
            uri = Uri.fromFile(patchedApp.getIconPath());
          }
          if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            Drawable foreground = Drawable.createFromPath(patchedApp.getIconPath().getAbsolutePath());
            AdaptiveIconDrawable adaptiveIcon = new AdaptiveIconDrawable(DISCORD_BACKGROUND, foreground);
            appIcon.setImageDrawable(adaptiveIcon);
          } else {
            appIcon.setImageURI(uri);
          }
        }
        appIcon.setOnClickListener(view12 -> {
          if (!Objects.requireNonNull(spiggyApp.getActivity()).isWorking()) {
            spiggyApp.getActivity().selectImage(patchedApp);
          } else {
            Toast.makeText(this.getContext(), R.string.already_patching, Toast.LENGTH_SHORT)
              .show();
          }
        });
        appButton.setOnLongClickListener(view12 -> {
          if (!Objects.requireNonNull(spiggyApp.getActivity()).isWorking()) {
            patchedApp.delete();
            return true;
          } else {
            Toast.makeText(this.getContext(), R.string.already_patching, Toast.LENGTH_SHORT)
              .show();
            return false;
          }
        });
      }
    });
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    binding = null;
  }

}