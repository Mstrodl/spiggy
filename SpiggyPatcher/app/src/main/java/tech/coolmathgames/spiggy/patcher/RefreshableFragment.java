package tech.coolmathgames.spiggy.patcher;

public interface RefreshableFragment {
  void refreshApps();
}
