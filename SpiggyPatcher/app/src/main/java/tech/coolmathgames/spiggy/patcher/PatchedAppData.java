package tech.coolmathgames.spiggy.patcher;

public class PatchedAppData {
  private String displayName;
  private String packageName;
  private String branch;
  private boolean hasIcon = false;
  private boolean installed = false;

  public PatchedAppData() {
  }

  public PatchedAppData(String displayName, String packageName, String branch, boolean hasIcon) {
    this();
    this.displayName = displayName;
    this.packageName = packageName;
    this.branch = branch;
    this.hasIcon = hasIcon;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getPackageName() {
    return packageName;
  }

  public void setPackageName(String packageName) {
    this.packageName = packageName;
  }

  public String getBranch() {
    return branch;
  }

  public void setBranch(String branch) {
    this.branch = branch;
  }

  public boolean getHasIcon() {
    return hasIcon;
  }

  public void setHasIcon(boolean hasIcon) {
    this.hasIcon = hasIcon;
  }

  public boolean getInstalled() {
    return this.installed;
  }

  public void setInstalled(boolean installed) {
    this.installed = installed;
  }
}
